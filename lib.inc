%define SYS_READ	0
%define SYS_WRITE 1
%define SYS_EXIT	60

%define IO_STDIN	0
%define IO_STDOUT 1
%define IO_STDERR 2

%macro JMP_IF_SPACE 2
	cmp	%1, ' '
	je	%2
	cmp	%1, 0x9
	je	%2
	cmp	%1, 0xA
	je	%2
%endmacro

%macro TAINT 1-*
%rep %0
	push	%1
%rotate 1
%endrep
%endmacro

%macro CLEAN 1-*
%rep %0
%rotate -1
	pop	%1
%endrep
%endmacro



section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov	rax, SYS_EXIT
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	mov	rax, rdi

	.loop:
	mov	dl, [rax]
	inc	rax
	test	dl, dl
	jnz	.loop

	dec	rax
	sub	rax, rdi
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	;; TAINT rbx; mov rbx, rdi; etc seems nicer but this is faster...
	TAINT	rdi
	call	string_length

	CLEAN	rsi
	mov	rdi, IO_STDOUT	; fd
	mov	rdx, rax	; len
	mov	rax, SYS_WRITE
	syscall

	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov	rdi, 0xA

; Принимает код символа и выводит его в stdout
print_char:
	dec	rsp
	
	mov	[rsp], dil

	mov	rdi, IO_STDOUT	; fd
	lea	rsi, [rsp]	; buf 
	xor	rdx, rdx
	inc	rdx	; len
	mov	rax, SYS_WRITE
	syscall

	inc	rsp
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test	rdi, rdi
	jns	print_uint
	
	TAINT	rdi
	mov	dil, '-'
	call	print_char
	CLEAN	rdi
	neg	rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	sub	rsp, 21
	
	mov	rax, rdi
	mov	r8, 10

	lea	rcx, [rsp+20]
	mov	byte [rcx], 0

	test	rax, rax
	jnz	.loop
	mov	rax, 0x30
	dec	rcx
	mov	byte [rcx], al
	jmp	.end

	.loop:
	test	rax, rax
	jz	.end

	xor	rdx, rdx
	div	r8
	
	add	rdx, 0x30
	dec	rcx
	mov	[rcx], dl

	jmp	.loop
	
	.end:
	lea	rdi, [rcx]
	call	print_string

	add	rsp, 21
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	TAINT	r12, r13, r14
	mov	r12, rdi
	mov	r13, rsi

	call	string_length
	mov	r14, rax

	mov	rdi, r13
	call	string_length
	
	cmp	rax, r14
	jne	.fail
	test	rax, rax
	jz	.ok

	mov	rcx, rax
	mov	rdi, r12
	mov	rsi, r13

	.loop:
	mov	dl, [rdi]
	xor	dl, [rsi]
	jnz	.fail

	inc	rdi
	inc	rsi
	loop	.loop

	.ok:
	xor	rax, rax
	inc	rax
	CLEAN	r12, r13, r14
	ret

	.fail:
	xor	rax, rax
	CLEAN	r12, r13, r14
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	dec	rsp

	mov	rax, SYS_READ
	xor	rdi, rdi	; fd
	mov	rsi, rsp	; buf
	xor	rdx, rdx
	inc	rdx	; len
	syscall
	
	test	rax, rax
	jz	.end

	xor	rax, rax
	mov	al, [rsp]

	.end:
	inc	rsp
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
read_word:
	TAINT	rbx, r12, r13
	mov	r12, rdi
	xor	r13, r13
	mov	rbx, rsi

	.ws:
	call	read_char
	JMP_IF_SPACE	al, .ws

	.loop:
	test	al, al
	jz	.ok
	cmp	rbx, r13
	jbe	.fail

	mov	byte [r12+r13], al

	call	read_char
	inc	r13

	JMP_IF_SPACE	al, .ok
	jmp	.loop


	.ok:
	mov	byte [r12+r13], 0
	mov	rax, r12
	mov	rdx, r13
	CLEAN	rbx, r12, r13
	ret

	.fail:
	xor	rax, rax
	CLEAN	rbx, r12, r13
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor	rax, rax	; the value
	xor	r8, r8		; char counter
	mov	r9, 10
	xor	rcx, rcx

	.loop:
	mov	cl, [rdi+r8]

	cmp	cl, '0'
	jb	.end
	cmp	cl, '9'
	ja	.end

	sub	cl, '0'
	mul	r9
	add	rax, rcx

	inc	r8
	jmp	.loop

	.end:
	mov	rdx, r8
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	TAINT	rbx, r12, r13
	mov	rbx, rdi
	mov	r12b, [rdi]  ; sign
	mov	r13, rdi

	cmp	r12b, '0'
	jb	.probsign
	cmp	r12b, '9'
	jbe	.do_parse
	jmp	.fail

	.probsign:
	cmp	r12b, '-'
	je	.oksign
	cmp	r12b, '+'
	jne	.fail
	.oksign:
	inc	r13

	.do_parse:
	mov	rdi, r13
	call	parse_uint
	test	rdx, rdx
	jz	.fail

	cmp	r13, rbx
	je	.end

	cmp	r12b, '-'
	jne	.nonneg
	neg	rax
	inc	rdx
	jmp	.end

	.nonneg:
	cmp	r12b, '+'
	jne	.fail
	inc rdx

	.end:
	CLEAN	rbx, r12, r13
	ret 

	.fail:
	xor	rdx, rdx
	CLEAN	rbx, r12, r13
	ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	TAINT	rbx, r12, r13
	mov	rbx, rdi
	mov	r12, rsi
	mov	r13, rdx

	call	string_length

	cmp	rax, r13
	jb	.ok

	xor	rax, rax
	CLEAN	rbx, r12, r13
	ret

	.ok:
	mov	rcx, rax
	inc	rcx
	mov	rsi, rbx
	mov	rdi, r12
	rep movsb

	CLEAN	rbx, r12, r13
	ret

; vim:ft=asm:sw=8:ts=8:noet
